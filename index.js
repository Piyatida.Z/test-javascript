const eximpleData = require("./example-data-1.json");

/**ข้อ 1 */
const findProdoct = eximpleData.find((product) => product.id == 10207);
console.log("1.--> " + findProdoct.name);
/** */



/**ข้อ 2 */
var sumArray = function (items, prop) {
  return items.reduce(function (a, b) {
    return a + b[prop];
  }, 0);
};

const totalPrice = sumArray(eximpleData, "price");
console.log("2.--> " + totalPrice);
/** */



/**ข้อ 3 */
const findVatproduct = eximpleData.filter(
  (product) => product.vat_percent != 0
);
console.log("3.--> " + (findVatproduct.length > 0 ? false : true));
/** */



/**ข้อ 5 */
var result = eximpleData.map((el) => {
  var o = Object.assign({}, el);
  o.isToggle = false;
  return o;
});

 console.log('5.--> ' + JSON.stringify(result));
/** */



/**ข้อ 10 */
let firstFunction = () =>
  new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve("1");
    }, 3000);
  });

let secondFunction = () =>
  new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve("2");
    }, 1000);
  });

let thirdFunction = () =>
  new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve("3");
    }, 2000);
  });

let promise = [firstFunction(), secondFunction(), thirdFunction()];
Promise.all(promise).then((resolve) => {
  console.log("" + resolve[0] + "\n" + resolve[1] + "\n" + resolve[2]);
});

/** */

const express = require("express");
const app = express();
const PORT = process.env.PORT || 8080;



app.get("/", (req, res) => {
  /**ข้อ 4 */
  let array = [];

   eximpleData.filter((product) => {
        let products = product["products"].filter((element) => element["price"] <= 200);
        if (products.length != 0 || product["products"].length == 0) {
             array.push(product);
        }
       });

   res.json(array);
  /** */



  /**ข้อ 6 */
   let array = [];

   eximpleData.filter(product=>{
       let newArray = {};
       newArray["name"] = product.name
     newArray["productsTotalPrice"] = sumArray(product.products,'price');
      array.push(newArray);
   })

   res.json(array);
  /** */



  /** ข้อ7 */
   let array = [];

   const val = eximpleData.filter(el=>el.is_editable_price === false).filter(product=>{
     let newArray = {};
     newArray["name"] = product.name;
     newArray["totalSubProductWeight"] = sumArray(product.products,'weight');;
     array.push(newArray);
   })

   res.json(array);
  /** */



  /**ข้อ 8 */
    let array = [];

       eximpleData.filter((product) => {
          product["products"].filter((element) => {
             if(element["type"] != null){
               if(element["type"].length != 0){
                 array.push(product);
               }
             }
          })
         });

        let index = eximpleData.findIndex(el=>el.id == array[0].id);

     res.send('' + index);
  /** */


  
  /** ข้อ9 */
   let array = [];

   eximpleData.filter((product) => {
     product["products"].filter((element) => {
       if (element["type"] != null) {
         let value = element["type"].filter(
           (e) => e.id != null && e.value != null
         );
         if (value.length > 0) {
           value.map(val=>{
             array.push(val.id)
           })
         }
       }
     });
   });

   res.send(array);
  /** */
});

app.listen(PORT, () => {
   console.log(`Server is running on port : ${PORT}`)
});

module.exports = app;
